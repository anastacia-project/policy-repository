# Use an official Python runtime as a parent image
FROM python:3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 8004 available to the world outside this container
EXPOSE 8004

# Define environment variable
#ENV NAME World

# Run app.py when the container launches
#CMD ["python", "policy_repository_service/manage.py", "runserver", "0.0.0.0:8004"] 
CMD ["/app/docker-entrypoint.sh"] 