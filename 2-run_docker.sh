#!/bin/bash

 USAGE=" USAGE: sudo ./2-run_docker.sh policy_repository_service 8004:8004"

if [ $# -eq 0 ]
  then
    echo $USAGE
    exit 1
fi

sudo docker run --name $1 -d -p $2 $1
