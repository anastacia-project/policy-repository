#!/bin/bash
#__author__ = "Alejandro Molina Zarca"
#__copyright__ = "Copyright 2018, ANASTACIA H2020"
#__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
#__license__ = "GPL"
#__version__ = "0.0.1"
#__maintainer__ = "Alejandro Molina Zarca"
#__email__ = "alejandro.mzarca@um.es"
#__status__ = "Development"

 USAGE="ANASTACIA USAGE examples:
    --General use--
 		./test_policy_repository_api.sh [ANASTACIA_FRAMEWORK_ADDR:PORT] [OPERATION] [PARAM]

    -- Get all mspl templates
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-templates

    -- Get specific mspl templates
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-templates capability=filtering_l4
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-templates capability=iot_control
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-templates capability=iot_honeynet

    -- Get all hspl instances
 		./test_policy_repository_api.sh anastacia-framework:8004 hspl-instances

    -- Get specific hspl instances
 		./test_policy_repository_api.sh anastacia-framework:8004 hspl-instances hspl_contains=auth

    -- Get all mspl instances
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-instances

    -- Get specific mspl instances
 		./test_policy_repository_api.sh anastacia-framework:8004 mspl-instances mspl_contains=auth

    -- Get all lowlevel instances
 		./test_policy_repository_api.sh anastacia-framework:8004 lowlevel-instances

    -- Get specific mspl instances
 		./test_policy_repository_api.sh anastacia-framework:8004 lowlevel-instances low_level_contains=drop

    -- Get all policy refinements 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-refinements

    -- Get specific policy refinements 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-refinements hspl_contains=auth
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-refinements mspl_contains=filter
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-refinements 'hspl_contains=auth&mspl_contains=filter'

    -- Get all policy translations 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-translations

    -- Get specific policy translations 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-translations mspl_contains=filter
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-translations low_level_contains=drop
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-translations 'mspl_contains=auth&low_level_contains=filter'

    -- Get all policy enforcements 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements

    -- Get specific policy enforcements 
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements mspl_contains=filter
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements status=p
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements status=e
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements status=d
 		./test_policy_repository_api.sh anastacia-framework:8004 policy-enforcements 'mspl_contains=auth&status=d'


 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

if [ ! -z "$3" ]
  then
    param="?$3"
fi

curl -H "Content-Type: application/json" -X GET http://$1/api/v1/$2/$param