# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the policy repository views inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

#from django.shortcuts import render
from rest_framework import viewsets, generics
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.response import Response
import django_filters

from .serializers import CapabilitySerializer, HSPLInstanceSerializer, MSPLInstanceSerializer, MSPLTemplateSerializer, LowLevelInstanceSerializer, PolicyRefinementSerializer, PolicyTranslationSerializer, PolicyEnforcementSerializer
from .models import Capability, HSPLInstance, MSPLInstance, MSPLTemplate, LowLevelInstance, PolicyRefinement, PolicyTranslation, PolicyEnforcement
import json, random
# Logging 
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class CapabilityViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = Capability.objects.all()
	serializer_class = CapabilitySerializer

class HSPLInstanceViewSetFilter(django_filters.FilterSet):
	hspl_contains = django_filters.CharFilter(name='hspl',lookup_expr='icontains')

	class Meta:
		model = HSPLInstance
		fields = ['hspl_contains']

class HSPLInstanceViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = HSPLInstance.objects.all()
	serializer_class = HSPLInstanceSerializer
	filter_class = HSPLInstanceViewSetFilter

class MSPLInstanceViewSetFilter(django_filters.FilterSet):
	mspl_contains = django_filters.CharFilter(name='mspl',lookup_expr='icontains')

	class Meta:
		model = MSPLInstance
		fields = ['mspl_contains']

class MSPLInstanceViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = MSPLInstance.objects.all()
	serializer_class = MSPLInstanceSerializer
	filter_class = MSPLInstanceViewSetFilter

class MSPLTemplateFilter(django_filters.FilterSet):
	#max_id = django_filters.NumberFilter(name="id", lookup_expr='lte')
	#closed_before = django_filters.DateFilter(name="close_date", lookup_expr='lte')
	#closed_after = django_filters.DateFilter(name="close_date", lookup_expr='gte')
	#expire_before = django_filters.DateFilter(name="respond_by_date", lookup_expr='lte')
	#expire_after = django_filters.DateFilter(name="respond_by_date", lookup_expr='gte')
	#net = django_filters.CharFilter(name="name", lookup_expr='iexact')
	capabilities = django_filters.CharFilter(lookup_expr='name__icontains')

	class Meta:
		model = MSPLTemplate
		fields = ['capabilities','mspl']

class MSPLTemplateAPIView(generics.ListAPIView):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = MSPLTemplate.objects.all()
	serializer_class = MSPLTemplateSerializer
	filter_class = MSPLTemplateFilter

class LowLevelInstanceViewSetFilter(django_filters.FilterSet):
	low_level_contains = django_filters.CharFilter(name='low_level',lookup_expr='icontains')

	class Meta:
		model = LowLevelInstance
		fields = ['low_level_contains']

class LowLevelInstanceViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = LowLevelInstance.objects.all()
	serializer_class = LowLevelInstanceSerializer
	filter_class = LowLevelInstanceViewSetFilter

class PolicyRefinementViewSetFilter(django_filters.FilterSet):
	hspl_contains = django_filters.CharFilter(name='hspl',lookup_expr='hspl__icontains')
	mspl_contains = django_filters.CharFilter(name='mspl',lookup_expr='mspl__icontains')

	class Meta:
		model = PolicyRefinement
		fields = ['hspl_contains','mspl_contains']

class PolicyRefinementViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = PolicyRefinement.objects.all()
	serializer_class = PolicyRefinementSerializer
	filter_class = PolicyRefinementViewSetFilter

	@action(methods=['post'], detail=False)
	def set_refinement(self, request):
		return Response({'status':'test'})

class PolicyTranslationViewSetFilter(django_filters.FilterSet):
	mspl_contains = django_filters.CharFilter(name='mspl',lookup_expr='mspl__icontains')
	low_level_contains = django_filters.CharFilter(name='low_level',lookup_expr='low_level__icontains')

	class Meta:
		model = PolicyTranslation
		fields = ['mspl_contains','low_level_contains']

class PolicyTranslationViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = PolicyTranslation.objects.all()
	serializer_class = PolicyTranslationSerializer
	filter_class = PolicyTranslationViewSetFilter


class PolicyEnforcementViewSetFilter(django_filters.FilterSet):
	"""
	Filter for the Enforcement View Set
	"""
	mspl_contains = django_filters.CharFilter(name='mspl',lookup_expr='mspl__icontains')
	status = django_filters.CharFilter(lookup_expr='icontains')

	class Meta:
		model = PolicyEnforcement
		fields = ['status','mspl_contains']


class PolicyEnforcementViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows HSPL instances to be viewed or edited.
	"""
	queryset = PolicyEnforcement.objects.all()
	serializer_class = PolicyEnforcementSerializer
	filter_class = PolicyEnforcementViewSetFilter


class SetPolicyRefinement(APIView):

	def post(self, request, format=None):
		
		body = request.stream.read().decode("utf-8")
		#body = request.body
		if not body:
			raise falcon.HTTPBadRequest('Empty request body',
										'A valid JSON document is required.')
		#print("body",body)
		hspl_mspl = json.loads(body)
		hspl = hspl_mspl["hspl"]
		hspl_instance = HSPLInstance(hspl=hspl)
		hspl_instance.save()

		mspl = hspl_mspl["mspl"]
		mspl_instance = MSPLInstance(mspl=mspl)
		mspl_instance.save()
		# Store the correspondence between them
		PolicyRefinement(hspl=hspl_instance,mspl=mspl_instance).save()

		"""
		for hspl_mspl in json_input["hspl_mspl"]:
			# Get hspl policy and register a new instantiation
			#print(hspl_mspl["hspl"])
			hspl = hspl_mspl["hspl"]
			hspl_instance = HSPLInstance(hspl=hspl)
			hspl_instance.save()

			# Get mspl policies and register a new instantiation
			#print(hspl_mspl["mspl_list"])
			mspl_list = hspl_mspl["mspl_list"]
			for mspl in mspl_list:
				mspl = mspl["mspl"]
				mspl_instance = MSPLInstance(mspl=mspl)
				mspl_instance.save()
				# Store the correspondence between them
				PolicyRefinement(hspl=hspl_instance,mspl=mspl_instance).save()
		"""

		# Create the hspl-mspl refinement
		return Response({'status':'Security policy refinement has been registered successfully'})

class SetPolicyTranslation(APIView):

	def post(self, request, format=None):
		
		body = request.stream.read().decode("utf-8")
		#body = request.body
		if not body:
			raise falcon.HTTPBadRequest('Empty request body',
										'A valid JSON document is required.')
		#print("body",body)
		json_input = json.loads(body)
		#logger.info(json_input)
		if "omspl_translation" in json_input.keys():
			omspl_translation = json_input["omspl_translation"]
			mspl_instance = MSPLInstance(mspl=omspl_translation["mspl"])
			mspl_instance.save()
			mspl_translations = omspl_translation["mspl_translations"]
		else:
			mspl_translations = json_input["mspl_translations"]

		for mspl_translation in mspl_translations:
			# Get mspl policy and register a new instantiation (if it does not exist)
			#logger.info(mspl_translation)
			mspl = mspl_translation["mspl"]
			mspl_instance = MSPLInstance(mspl=mspl)
			mspl_instance.save()

			# Get mspl policy and register a new instantiation
			#print(mspl_low["low_level"])
			low_level = mspl_translation["enabler_conf"]
			low_level_instance = LowLevelInstance(low_level=low_level)
			low_level_instance.save()

			# Store the correspondence between them
			PolicyTranslation(mspl=mspl_instance,low_level=low_level_instance).save()

		# Create the hspl-mspl refinement
		return Response({'status':'Security policy translation has been registered successfully'})

class SetPolicyEnforcement(APIView):

	def post(self, request, format=None):
		
		body = request.stream.read().decode("utf-8")
		#body = request.body
		if not body:
			raise falcon.HTTPBadRequest('Empty request body',
										'A valid JSON document is required.')
		#print("body",body)
		json_input = json.loads(body)
		for mspl_status in json_input["mspl_status"]:
			mspl_id = mspl_status["mspl_id"]
			status = mspl_status["status"]
			#logger.info("RECEIVED IN THE POLICY REPOSITORY:",mspl_id,status)

			# Get mspl_id, load the object
			#mspl = MSPLInstance.objects.get(pk=mspl_id)
			# FAKE FOR DEMO
			mspl = MSPLInstance.objects.filter(mspl__icontains=mspl_id).exclude(mspl__icontains="ITResourceOrchestration")
			if mspl:
				mspl=mspl[0]
			#mspl = random.choice(mspl_objects)
			#print(mspl)
			#print(status)
			# Store the correspondence between them
			PolicyEnforcement(mspl=mspl,status=status).save()

		# Create the hspl-mspl refinement
		return Response({'status':'Security policy status has been registered successfully'})
