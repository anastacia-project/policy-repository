# # -*- coding: utf-8 -*-
# serielizers.py
"""
This python module implements the policy repository serializers inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from .models import Capability, HSPLInstance, MSPLInstance, MSPLTemplate, LowLevelInstance, PolicyRefinement, PolicyTranslation, PolicyEnforcement
from rest_framework import serializers

class CapabilitySerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Capability
		fields = ('id','name')

class HSPLInstanceSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = HSPLInstance
		fields = ('hspl','creation_date')

class MSPLInstanceSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = MSPLInstance
		fields = ('mspl','creation_date')

class MSPLTemplateSerializer(serializers.HyperlinkedModelSerializer):

	capabilities = CapabilitySerializer(many=True)

	class Meta:
		model = MSPLTemplate
		fields = ('id','mspl','capabilities','creation_date')

class LowLevelInstanceSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = LowLevelInstance
		fields = ('low_level','creation_date')

class PolicyRefinementSerializer(serializers.HyperlinkedModelSerializer):

	hspl = HSPLInstanceSerializer()
	mspl = MSPLInstanceSerializer()

	class Meta:
		model = PolicyRefinement
		fields = ('id','hspl', 'mspl','creation_date')

class PolicyTranslationSerializer(serializers.HyperlinkedModelSerializer):

	mspl = MSPLInstanceSerializer()
	low_level = LowLevelInstanceSerializer()

	class Meta:
		model = PolicyTranslation
		fields = ('id','mspl', 'low_level', 'creation_date')

class PolicyEnforcementSerializer(serializers.HyperlinkedModelSerializer):

	mspl = MSPLInstanceSerializer()

	class Meta:
		model = PolicyEnforcement
		fields = ('id','mspl','status','creation_date')