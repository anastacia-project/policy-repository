# # -*- coding: utf-8 -*-
# views.py
"""
This python module implements the policy repository urls inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


from django.conf.urls import url, include
from rest_framework import routers
from policy_repository import views

router = routers.DefaultRouter()
router.register(r'capabilities', views.CapabilityViewSet)
router.register(r'hspl-instances', views.HSPLInstanceViewSet)
router.register(r'mspl-instances', views.MSPLInstanceViewSet)
#router.register(r'mspl-templates', views.MSPLTemplateViewSet)
router.register(r'lowlevel-instances', views.LowLevelInstanceViewSet)
router.register(r'policy-refinements', views.PolicyRefinementViewSet)
#router.register(r'set_refinement', views.SetPolicyRefinement.as_view(), name="set_refinement")
router.register(r'policy-translations', views.PolicyTranslationViewSet)
router.register(r'policy-enforcements', views.PolicyEnforcementViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^set-policy-refinement/$', views.SetPolicyRefinement.as_view()),
    url(r'^set-policy-translation/$', views.SetPolicyTranslation.as_view()),
    url(r'^set-policy-enforcement/$', views.SetPolicyEnforcement.as_view()),
    url(r'^mspl-templates/$', views.MSPLTemplateAPIView.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]