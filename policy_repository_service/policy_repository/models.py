# # -*- coding: utf-8 -*-
# models.py
"""
This python module implements the policy repository models inside the ANASTACIA European Project,
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

from django.db import models

class Capability(models.Model):
	name = models.TextField()

class HSPLInstance(models.Model):
	hspl = models.TextField()
	creation_date = models.DateTimeField(auto_now_add=True)

class MSPLInstance(models.Model):
	mspl = models.TextField()
	creation_date = models.DateTimeField(auto_now_add=True)
	#source? HSPL/REACTION 

class LowLevelInstance(models.Model):
	low_level = models.TextField()
	creation_date = models.DateTimeField(auto_now_add=True)

class HSPLTemplate(models.Model):
	hspl = models.TextField()
	creation_date = models.DateTimeField(auto_now_add=True)

class MSPLTemplate(models.Model):
	mspl = models.TextField()
	capabilities = models.ManyToManyField(Capability)
	creation_date = models.DateTimeField(auto_now_add=True)

class PolicyRefinement(models.Model):
	hspl = models.ForeignKey(HSPLInstance, on_delete=models.CASCADE)
	mspl = models.ForeignKey(MSPLInstance, on_delete=models.CASCADE)
	creation_date = models.DateTimeField(auto_now_add=True)

class PolicyTranslation(models.Model):
	mspl = models.ForeignKey(MSPLInstance, on_delete=models.CASCADE)
	low_level = models.ForeignKey(LowLevelInstance, on_delete=models.CASCADE)
	creation_date = models.DateTimeField(auto_now_add=True)


class PolicyEnforcement(models.Model):
	PENDING = 'P'
	ENFORCED = 'E'
	DELETED = 'D'
	POLICY_ENFORCEMENT_STATUS = [(PENDING,'Pending'),(ENFORCED,'Enforced'),(DELETED,'Deleted')]
	mspl = models.ForeignKey(MSPLInstance, on_delete=models.CASCADE)
	status = models.CharField(max_length=1, choices=POLICY_ENFORCEMENT_STATUS, default=PENDING)
	creation_date = models.DateTimeField(auto_now_add=True)

