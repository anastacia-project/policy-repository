from django.apps import AppConfig


class PolicyRepositoryConfig(AppConfig):
    name = 'policy_repository'
